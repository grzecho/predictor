
import torch
import predictor_rnn as pr
import matplotlib.pyplot as plt
from datetime import datetime

now = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

number_of_hidden_neurons = 50
hidden_layers = 10
training_iterations = 10000



dataset_tensor, dataset_results, targetx, shift, input_dataframe = pr.generateDataset(80, 12, 30, tensor_device=device, flatten_tensor=False)
#input_length = len(dataset_tensor[1,:])

net = pr.Net(6, number_of_hidden_neurons, hidden_layers, 1, device)
net = net.to(device)

result = net.forward(dataset_tensor)

print(result)
params = list(net.parameters())
print(len(params))


test_tensor, test_result, test_targetx, test_shift, test_dataframe = pr.generateDataset(15, 12, 30, tensor_device=device,flatten_tensor=False)

[trained_net, best_epoch, best_mse] = pr.trainNetwork(net, dataset_tensor, dataset_results, test_tensor, test_result, training_iterations)
torch.save(net.state_dict(), '../../results/models/OptimizedRNN-' + str(number_of_hidden_neurons) + 'hidden.pt')


#print(net.fc1.weight)
#print(full_output)
print(trained_net.parameters)

print("Net prediction:")
pred = trained_net.forward(test_tensor).data
print(pred)

print("Real value:")
print(test_result)

print("Graph shift:")
print(test_shift)

plt.scatter(test_dataframe[0]['x'], test_dataframe[0]['val'],color='r', label='provided graph points')
plt.scatter([test_targetx[0]], test_result.tolist()[0], color='b', label='point to predict')
plt.scatter([test_targetx[0]], pred.tolist()[0], color='c', label='predicted value')
plt.title(f"Epoch: {best_epoch}, MSE: {best_mse:.3f}, # of h-neurons: {number_of_hidden_neurons}, # of h-layers: {hidden_layers}")
plt.legend()
plt.savefig('../../results/plots/RNN-' + now + '_' + str(number_of_hidden_neurons) + '-hidden-neurons.png', format='png')


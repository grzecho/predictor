from typing import List, Any

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import math
import datasetgen as gen
import copy

class Net(nn.Module):

    def __init__(self, inputs: int=6, outputs: int=1, hidden: int=100, layers: int=1, dev: str="cpu"):
        super(Net, self).__init__()

        hidden_per_layer = int(math.ceil(hidden / layers))

        self.fc = nn.ModuleList([nn.Linear(inputs, hidden_per_layer)])
        for layer in range(2, layers+1):
            self.fc.append(nn.Linear(hidden_per_layer,hidden_per_layer).to(dev))
        self.fc.append(nn.Linear(hidden_per_layer,outputs).to(dev))

        self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax(dim=0)
        self.relu = nn.ReLU()

        for layer in self.fc:
            nn.init.normal_(layer.weight, mean=0, std=1)

        
    def forward(self, x):
        for layer in self.fc:
            x = layer(x)
            if layer != self.fc[len(self.fc)-1]:
                x = self.sigmoid(x)
        
        return x

def generateDataset(numberofsamples = 150, lengthofgraph = 12, data_points = 40, tensor_device = "cpu", flatten_tensor=True):
   full_numpy = np.array([[]])
   inputdf = []   
   random_shift = []
   targetx = []  
   output = []

   for l in range(numberofsamples):

      graph_length = lengthofgraph
      random_shift.append(np.random.random()*2*math.pi)
      random_squeeze = np.random.random() + 1
      random_climb = np.random.random()*2 - 1

      targetx.append(np.random.random()*3 + graph_length)
      output.append([math.sin(random_squeeze*targetx[l]+random_shift[l]) + targetx[l]*random_climb])

      inputdf.append(gen.randomsinuspoints(data_points, graph_length,random_shift[l],random_squeeze, random_climb))
      inputdf[l] = gen.gradientfromdataframe(inputdf[l])
      inputdf[l] = gen.distanceindataframe(inputdf[l])
      inputdf[l] = gen.distancefromtargetindataframe(inputdf[l], targetx[l])
      inputdf[l] = inputdf[l].fillna(0)


      #mean = inputdf[l]['val'].mean(axis=0)
      #maxval = inputdf[l]['val'].max(axis=0)
      #minval = inputdf[l]['val'].min(axis=0)

      df_numpy = inputdf[l].to_numpy()
      if flatten_tensor == True:
          df_numpy = df_numpy.flatten()
      np.append(df_numpy, targetx[l])

      if len(full_numpy.flat) > 0:
         full_numpy = np.concatenate((full_numpy, [df_numpy]),axis=0)
      else:
         full_numpy = np.expand_dims(df_numpy,axis=0)


   full_output = torch.Tensor(output).to(tensor_device)
   input_tensor = torch.from_numpy(full_numpy).type(torch.float).to(tensor_device)
   return input_tensor, full_output, targetx, random_shift, inputdf


def trainNetwork(network, dataset_tensor, dataset_results, new_dataset_tensor, new_dataset_results, steps = 1000):
   lr = 1e-4
   counter = 0
   criterion = nn.MSELoss(reduction='sum')
   #optimizer = torch.optim.SGD(network.parameters(), lr=lr)
   optimizer = torch.optim.Adam(network.parameters())

   best_so_far_error = math.inf
   last_loss = math.inf
   best_after_epoch = 0

   for t in range(steps):
       # Forward pass: Compute predicted y by passing x to the model
       y_pred = network.forward(dataset_tensor)
       y_pred_new_dataset = network.forward(new_dataset_tensor)       

       # Compute and print loss
       loss = criterion(y_pred, dataset_results)
       new_dataset_loss = criterion(y_pred_new_dataset, new_dataset_results)
       #print(t, loss.item(), y_pred, output)
       print(t, loss.item(), new_dataset_loss.item(), best_so_far_error)
       
       if new_dataset_loss.item() < best_so_far_error:
          best_so_far_error = new_dataset_loss.item()
          best_after_epoch = t
          best_so_far_model = copy.deepcopy(network)       

       if loss.item() >= last_loss:
           counter = counter + 1
           if counter == math.ceil(steps / 500):
               lr = lr / 10
               counter = 0
               if lr < 1e-10:
                   break
               for g in optimizer.param_groups:
                   g['lr'] = lr

       # Zero gradients, perform a backward pass, and update the weights.
       optimizer.zero_grad()
       loss.backward()
       optimizer.step()

       last_loss = loss.item()

   print(f"Current loss {lr:.1e}")

   return best_so_far_model, best_after_epoch, best_so_far_error



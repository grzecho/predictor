import numpy as np
import pandas as pd

def randomsinuspoints(numofelements, breadth, start=0, squeeze=1, climb=0):
   randomnums = np.random.random(numofelements) * breadth
   results = np.sin(squeeze*randomnums + start) + randomnums*climb
   series = {'x':randomnums, 'val':results}
   df = pd.DataFrame(series).sort_values(by='x')
   reindexed = df.reset_index(drop=True)
   return reindexed

def gradientfromdataframe(existing):
   gradientList = []
   gradientListPerUnit = []
   lastrow = next(existing.iterrows())[1]
   for index, row in existing.iterrows():
      gradientList.append(lastrow['val'] - row['val'])
      gradientListPerUnit.append((row['val'] - lastrow['val']) / (row['x'] - lastrow['x']))
      lastrow = row
   existing['gradient'] = gradientList
   existing['gradient per unit'] = gradientListPerUnit
   return existing


def distanceindataframe(existing):
   distanceList = []
   lastrow = next(existing.iterrows())[1]
   for index, row in existing.iterrows():
      distanceList.append(lastrow['x'] - row['x'])
      lastrow = row
   existing['distance'] = distanceList
   return existing

def distancefromtargetindataframe(existing, targetx):
   distanceList = []
   for index, row in existing.iterrows():
      distanceList.append(targetx - row['x'])
   existing['distance from target'] = distanceList
   return existing

